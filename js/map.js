
var locations = [];
var marker, i;
var activeInfoWindow; 

      function initMap() {
        var uluru = {lat: 31.762234, lng:  35.218759};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 14,
          center: uluru
        });

map.setOptions({
    styles: [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
]});

 locations = [
      ['HAMESILA', 31.76338, 35.21994, '<div class="location-info"><img src="img/hamesila.png" width="150" class="img-responsive"></div>',0], 
      ['Park 8', 31.76161, 35.22115, '<div class="location-info"><img src="img/map_places/park8.jpg" width="150" class="img-responsive"></div>',1],     
      ['Western Wall', 31.77671, 35.2345, '<div class="location-info"><img src="img/map_places/westernwall.jpg" width="150" class="img-responsive"></div>',2],
      ['Yemin Moshe', 31.77413, 35.22483, '<div class="location-info"><img src="img/map_places/yeminmoshe.jpg" width="150" class="img-responsive"></div>',3],
      ['Jerusalem Theater', 31.769, 35.21563, '<div class="location-info"><img src="img/map_places/jeruslemtheater.jpg" width="150" class="img-responsive"></div>',4],
      ['The First Satation', 31.76721, 35.22428, '<div class="location-info"><img src="img/map_places/fIrststation.jpg" width="150" class="img-responsive"></div>',5],
      ['Orient Hotel Jerusalem', 31.76663,35.22316, '<div class="location-info"><img src="img/map_places/orienthoteljerusalem.jpg" width="150" class="img-responsive"></div>',6],
      ['Gabriel Scherover Promenade', 31.75864,35.22753, '<div class="location-info"><img src="img/map_places/gabrielscheroverpromnade.jpg" width="150" class="img-responsive"></div>',7],
      ['HaMesila Park', 31.76244,35.21879, '<div class="location-info"><img src="img/map_places/place-7.jpg" width="150" class="img-responsive"></div>',8],
      ['Hadar Mall', 31.75355, 35.2135, '<div class="location-info"><img src="img/map_places/hadarmall.jpg" width="150" class="img-responsive"></div>',9],
      ['Inbal Jerusalem Hotel', 31.77062, 35.22181, '<div class="location-info"><img src="img/map_places/inbalhotel.jpg" width="150" class="img-responsive"></div>',10],
      ['King David Hotel',31.77437, 35.22261, '<div class="location-info"><img src="img/map_places/kingdavidhotel.jpg" width="150" class="img-responsive"></div>',11],
      ['David Citadel Hotel', 31.77711, 35.2229, '<div class="location-info"><img src="img/map_places/davidcitadel.jpg" width="150" class="img-responsive"></div>',12],
      ['Alrov Mamilla Avenue', 31.77772, 35.2238, '<div class="location-info"><img src="img/map_places/alrovmamillaavenue.jpg" width="150" class="img-responsive"></div>',13]
    ];

var infowindow = new google.maps.InfoWindow();
var locationlist = '';


for(i=0;i<locations.length;i++){

  if((locations[i][0] == 'HAMESILA')||(locations[i][0] == 'Park 8')){
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(locations[i][1], locations[i][2]),
      map: map,
      icon: 'img/map_places/0'+(i+1)+'.png'
    });
  }else{

   marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map,
        icon: 'img/map_places/'+(i-1)+'.png'
      });
    }

    google.maps.event.addListener(marker, 'click', (function showimg(marker, i) {
      
      return function() {
        $('#location-list p').eq(i).trigger('click');
      }
    })(marker, i));

   if((locations[i][0] == 'HAMESILA')||(locations[i][0] == 'Park 8')){
    locationlist +='<div class="row"><div class="col-md-2 col-xs-2"><img src="img/map_places/0'+(i+1)+'.png" class="img-responsive" style="float:right;"></div><div class="col-md-8 col-xs-8"><p onclick="setLocation(this)" data="'+locations[i][4]+'" style="cursor:pointer;">'+locations[i][0]+'</p></div></div>';
   }else{
locationlist +='<div class="row"><div class="col-md-2 col-xs-2"><img src="img/map_places/'+(i-1)+'.png" class="img-responsive" style="float:right;"></div><div class="col-md-8 col-xs-8"><p onclick="setLocation(this)" data="'+locations[i][4]+'" style="cursor:pointer;">'+locations[i][0]+'</p></div></div>';}
}

$('#location-list').html(locationlist);
}

   function setLocation(index){
    if(infowindow){
      infowindow.close();
    }
    $('#location-list').find('.activepoint').removeClass('activepoint'); 
    index.classList.add("activepoint");

    var i = index.getAttribute("data");
    var infowindow = new google.maps.InfoWindow();

        infowindow.setContent('<p>'+locations[i][0]+'</p>'+locations[i][3]);
        var latlng = new google.maps.LatLng(locations[i][1], locations[i][2]);
        marker.setPosition(latlng);
        if (activeInfoWindow ) { activeInfoWindow.close();}
        infowindow.open(map, marker);
        var target = $('#map').offset().top;

        $('html,body').animate({
          scrollTop: target - 130
        }, 1000);
        activeInfoWindow = infowindow;
     }

       



