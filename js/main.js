$(document).ready(function(){
    //  $('[data-fancybox="video"]').fancybox({
    //   buttons: [
    //     "close"
    // ],
    //   }).trigger('click');;
    $('.videocontent').click(function(){
      console.log('trigger');
      $('.videocontent').hide();
      var player = $("#hamesilavideo");
      froogaloop = $f(player[0].id);

          froogaloop.api('play');
    })
    $("#submitContact").click(function () {
      err = false;
      $("#contactForm .req").each(function () {
          if (!$(this).val()) {
              err = true;
          }
      });
      if (err) {
          return false;
      } else {
          submitEmail();
          return false;
      }
  });
  function submitEmail() {
      $.ajax({
          method: "POST",
          url: "mailit.php",
          data: {firstname: $("input[name='firstname']").val(), lastname: $("input[name='lastname']").val(), email: $("input[name='email']").val(), phone: $("input[name='phone']").val(), message: $("textarea[name='message']").val()}
      })
              .done(function (msg) {
                  if (msg === "1") {
                    $('#formwrapper').fadeOut(500, function(){
                      $('#thankyou').fadeIn();
                    });
                    

                  }
              });
  }

    $('.slick').slick({
        dots: true,
        infinite: true,
        autoplaySpeed: 5000,
        fade: true,
        autoplay: false,
        lazyLoad: 'ondemand',
        cssEase: 'linear',
        arrows: false
    });
    $('.slick-luxury').slick({
      dots: true,
      infinite: true,
      autoplaySpeed: 5000,
      fade: true,
      autoplay: true,
      lazyLoad: 'ondemand',
      cssEase: 'linear',
      arrows: false
  });
    $('.slick').on('afterChange', function(event, slick, currentSlide){
    if ( currentSlide != 0 ){
      
      var player = $("#hamesilavideo");
      froogaloop = $f(player[0].id);

          froogaloop.api('pause');
      }
    });
      $('#subForm').submit(function(e) {
          e.preventDefault();
          $.getJSON(
              this.action + "?callback=?",
              $(this).serialize(),
              function(data) {
                  if (data.Status === 400) {
                      alert("Error: " + data.Message);
                  } else { // 200
                      $('#formwrapper').hide();
                      $('#thankyou').show();
                      $('#subForm').trigger("reset");
                  }
              });
      });

    $(window).scroll(function() {
        if ($(document).scrollTop() > 50) {
          vAlign();
          $('nav').addClass('shrink');
          $('.navbar-brand img').attr('src', 'img/hamesila-smallv2.png')
        } else {
          $('nav').removeClass('shrink');
          $('.navbar-brand img').attr('src', 'img/hamesilav2.png')
        }
      });

      $('#btnfloorplans').on('click', function(e){
        e.preventDefault();
        $(this).toggleClass('activeamenities');
        $(this).text($(this).text() === 'View floor plans' ? "Hide Floor Plans" : "View Floor Plans");
        if($('#details').is(':empty')){
          showFloorplans();
        }
        $('#floorplanscontainer').slideToggle();
        var target = $('#floorplanscontainer').offset().top
        $('html,body').animate({
          scrollTop: target - 131
        }, 1000);
      });

      $('#btnamenities').on('click', function(e){
        e.preventDefault();

        $(this).toggleClass('activeamenities');
        if(!$('.slickamenities').hasClass('slick-initialized')){
          $('.slickamenities').slick(getSliderSettings());
        }
        $('#amenitiescontainer').slideToggle();
        var target = $('#amenitiescontainer').offset().top
        $('html,body').animate({
          scrollTop: target - 131
        }, 1000);
      });

      $(window).on("load", function (e){
        vAlign()
      })

           $(window).on("resize", function (e){
        vAlign()
      }) 
});

function showFloorplans(){
  var items = '';
  $.getJSON( "js/suites.json", {
  })
  .done(function(data){
    $.each(data, function(index, model ) {
      items += '<div class="col-md-4 col-sm-6 col-xs-12 element-item '+model.bedroom+' " data-fancybox data-animation-duration="700" data-src="./floorplans/'+ model.fp+'.pdf" href="javascript:;" data-bedroom="'+model.bedroom+'" style="margin-top:25px;"><div style="border:1px solid #312c32;padding:10px;"><div class="suites-box" style="background-color:' 
      + model.color+';"><div class="apt">APT ' + model.apt +'</div><div class="detail"><p class="text-uppercase">' + model.floor +'</p><p><b>SIZE</b> : '+ model.in + ' SQ METERS</p><!--p>EXTERIOR : ' + model.ex +' SQ METERS</p--></div></div></div></div>';
    });
    $('#details').html(items);
    

    IsotopeFloorplans();
  })
}
function getSliderSettings() {
  return {
    dots: true,
    infinite: true,
    autoplaySpeed: 5000,
    fade: true,
    autoplay: true,
    lazyLoad: 'ondemand',
    cssEase: 'linear',
    arrows: false
  };
}

function vAlign(inParent) {
  if(!isXS()){
  setTimeout(function () {
      $(".valign").each(function () {
          parentH = !inParent ? $(this).parent().height() / 2 : inParent;
          thsH = $(this).height() / 2;
          $(this).css("margin-top", parentH - thsH + "px");
      });
  }, 200);
}
}
function IsotopeFloorplans(){
  var $grid = $('.floorplangrid').isotope({
    itemSelector: '.element-item',
    layoutMode: 'fitRows'
    });

    $('.filters-button-group').on('click', 'button', function() {
      $('.button').removeClass('active');
      $(this).addClass('active');
      var filterValue = $( this ).attr('data-filter');
      // use filterFn if matches value
      $grid.isotope({ filter: filterValue });
    });

    $('.select-filter-group').change(function() {
      $(this).find('.active').removeClass('active');
      $(this).find(':selected').addClass('active');
      // $(this).addClass('active');
      var filterValue = $(this).find(':selected').attr('data-filter');
      // use filterFn if matches value
      $grid.isotope({ filter: filterValue });
    });
}

function isXS() {
  return $("#is-xs").css("display") === "block" ? true : false;
}

/*SCROLLING NAV*/
(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    console.log('trigger');
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 40)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 54
  });

})(jQuery); // End of use strict